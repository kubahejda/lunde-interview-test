#Lundegaard interview test
This is an example application allowing user to send his request to be stored.

##Requirements
* java 8
* maven

##Build and deploy steps
* mvn clean package
* java -jar target/interview-test-1.0.1-SNAPSHOT.jar

or

* mvn spring-boot:run 

##Description
The application itself starts at [localhost:8080](localhost:8080), where can be found the UI. The swagger-ui is reachable at [localhost:8080/swagger-ui.html](localhost:8080/swagger-ui.html) (where you can easily check inserted data) and the swagger API description JSON at [localhost:8080/v2/api-docs](localhost:8080/v2/api-docs)