Vue.use(VeeValidate);

var app = new Vue({
    el: '#contents',
    data: {
        formData: {
            email: "",
            questionType: "",
            policyNumber: "",
            firstName: "",
            lastName: "",
            requestText: ""
        },
        selected: "",
        questionTypesSelect: [],
        success: false,
        formArgumentsWarning: false,
        serverError: false

    },
    methods: {
        handleSubmit() {
            this.success = false;
            this.formArgumentsWarning = false;
            this.serverError = false;
            this.$validator.validate().then(result => {
                if (!result) {
                    this.formArgumentsWarning = true;
                } else {
                    this.processValidatedForm();
                }
            });
        },
        processValidatedForm() {
            for (obj in questionTypes) {
                if (questionTypes[obj] === this.selected) {
                }
                this.formData.questionType = obj;
            }

            this.$http.put("/api/v1/user-question", this.formData)
                .then(() => {
                    this.success = true;
                    console.log("OK");
                }, (err) => {
                    this.serverError = true;
                    console.log("Error", err);
                })
                .catch((e) => {
                    this.serverError = true;
                    console.log("Caught:", e);
                });
        }
    },
    created: function () {
        for (var propName in questionTypes) {
            this.questionTypesSelect.push(questionTypes[propName])
        }
    }
});