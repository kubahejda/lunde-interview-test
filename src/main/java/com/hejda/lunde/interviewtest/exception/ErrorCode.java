package com.hejda.lunde.interviewtest.exception;

public enum ErrorCode {
    QUESTION_NOT_FOUND(1001, ErrorFamily.OBJECT_NOT_FOUND, "User question not found");

    private final int code;
    private final ErrorFamily family;
    private final String defaultMessage;

    ErrorCode(Integer code, ErrorFamily family, String defaultMessage) {
        this.code = code;
        this.family = family;
        this.defaultMessage = defaultMessage;
    }

    public int getCode() {
        return code;
    }

    public ErrorFamily getFamily() {
        return family;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }
}
