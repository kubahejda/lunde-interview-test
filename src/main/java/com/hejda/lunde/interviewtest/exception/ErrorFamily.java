package com.hejda.lunde.interviewtest.exception;

public enum ErrorFamily {
    INVALID_REQUEST(400),
    USER_NOT_AUTHORIZED(401),
    OBJECT_NOT_FOUND(404),
    CONFLICT(409),
    INTERNAL_LOGIC_ERROR(500),
    BAD_GATEWAY(502);
    private final int defaultStatusCode;

    ErrorFamily(int defaultStatusCode) {
        this.defaultStatusCode = defaultStatusCode;
    }

    public int getDefaultStatusCode() {
        return defaultStatusCode;
    }
}

