package com.hejda.lunde.interviewtest.exception;

import java.io.Serializable;

public class ITException extends RuntimeException {
    private final ErrorCode errorCode;
    private final Serializable detail;

    public ITException(ErrorCode errorCode, Serializable detail, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
        this.detail = detail;
    }

    public ITException(ErrorCode errorCode, Serializable detail, Throwable cause) {
        this(errorCode, detail, errorCode.getDefaultMessage(), cause);
    }

    public ITException(ErrorCode errorCode, Serializable detail, String message) {
        this(errorCode, detail, message, null);
    }

    public ITException(ErrorCode errorCode, Serializable detail) {
        this(errorCode, detail, errorCode.getDefaultMessage(), null);
    }

    public ITException(ErrorCode errorCode, String message, Throwable cause) {
        this(errorCode, null, message, cause);
    }

    public ITException(ErrorCode errorCode, Throwable cause) {
        this(errorCode, null, errorCode.getDefaultMessage(), cause);
    }

    public ITException(ErrorCode errorCode, String message) {
        this(errorCode, null, message, null);
    }

    public ITException(ErrorCode errorCode) {
        this(errorCode, null, errorCode.getDefaultMessage(), null);
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public Serializable getDetail() {
        return detail;
    }
}
