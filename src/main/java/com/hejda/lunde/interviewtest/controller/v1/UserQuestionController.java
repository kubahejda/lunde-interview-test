package com.hejda.lunde.interviewtest.controller.v1;

import com.hejda.lunde.interviewtest.model.dto.UserQuestionRequestDto;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionResponseDto;
import com.hejda.lunde.interviewtest.service.impl.UserQuestionServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/v1/user-question")
@Api(value = "UserQuestionAPI", description = "Handles operations on UserQuestions.")
public class UserQuestionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserQuestionController.class);
    private static final String REQUEST_BODY_ATTRIBUTE = "requestBody";

    @Autowired
    private UserQuestionServiceImpl service;

    @ApiOperation(value = "Add a new user's question", response = UserQuestionResponseDto.class)
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserQuestionResponseDto> insertUserQuestion(@RequestBody @Valid UserQuestionRequestDto requestDto, WebRequest webRequest) {

        webRequest.setAttribute(REQUEST_BODY_ATTRIBUTE, requestDto, RequestAttributes.SCOPE_REQUEST);
        return new ResponseEntity<>(service.insertUserQuestion(requestDto), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Gets an user's question by ID.", response = UserQuestionResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Question data"),
            @ApiResponse(code = 404, message = "Question data not found")
    })
    @RequestMapping(path = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserQuestionResponseDto> getUserRequest(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getUserQuestion(id), HttpStatus.OK);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleError(ServletWebRequest request, Exception ex) {
        final HttpMethod httpMethod = request.getHttpMethod();
        final boolean expectingRequestBody = httpMethod.equals(HttpMethod.GET)
                || httpMethod.equals(HttpMethod.HEAD)
                || httpMethod.equals(HttpMethod.DELETE);
        final String requestBody = expectingRequestBody ? ""
                : request.getAttribute(REQUEST_BODY_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST).toString();

        LOGGER.error("Failed to handle {} at: {} with payload: {}, reason: ",
                httpMethod.name(),
                request.getRequest().getRequestURL(),
                requestBody,
                ex);

        return new ResponseEntity<>("An error occured while processing the request", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> responseBuilder(HttpStatus status, Object entity) {
        return new ResponseEntity<>(entity, status);
    }
}
