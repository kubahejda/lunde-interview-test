package com.hejda.lunde.interviewtest.controller;

import com.hejda.lunde.interviewtest.model.entity.QuestionType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
    private static final String QUESTION_TYPES_ATTRIBUTE = "questionTypes";
    private static final String INDEX_PAGE_QUALIFIER = "index";

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute(QUESTION_TYPES_ATTRIBUTE, QuestionType.getPossibleValues());
        return INDEX_PAGE_QUALIFIER;
    }
}
