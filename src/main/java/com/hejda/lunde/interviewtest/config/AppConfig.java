package com.hejda.lunde.interviewtest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class AppConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hejda.lunde.interviewtest.controller.v1"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }

    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("templates/");
        return resolver;
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("User Request REST API")
                .description("\"Spring Boot REST API for User question information handling\"")
                .version("1.0.0")
                .contact(new Contact("Jakub Hejda", "https://jakub.hejda", "hejda.jakub@gmail.com"))
                .build();
    }
}
