package com.hejda.lunde.interviewtest.model.builder;

import com.hejda.lunde.interviewtest.model.dto.UserQuestionRequestDto;
import com.hejda.lunde.interviewtest.model.entity.QuestionType;

public class UserQuestionRequestDtoBuilder {
    private QuestionType questionType;
    private String email;
    private String policyNumber;
    private String firstName;
    private String lastName;
    private String requestText;

    public UserQuestionRequestDtoBuilder setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
        return this;
    }

    public UserQuestionRequestDtoBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserQuestionRequestDtoBuilder setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
        return this;
    }

    public UserQuestionRequestDtoBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserQuestionRequestDtoBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserQuestionRequestDtoBuilder setRequestText(String requestText) {
        this.requestText = requestText;
        return this;
    }

    public UserQuestionRequestDto createUserQuestionRequestDto() {
        return new UserQuestionRequestDto(questionType, email, policyNumber, firstName, lastName, requestText);
    }
}