package com.hejda.lunde.interviewtest.model.builder;

import com.hejda.lunde.interviewtest.model.entity.Contact;
import com.hejda.lunde.interviewtest.model.entity.QuestionType;
import com.hejda.lunde.interviewtest.model.entity.UserQuestion;

import java.time.LocalDateTime;

public class UserQuestionEntityBuilder {
    private QuestionType questionType;
    private String requestText;
    private LocalDateTime createdAt;
    private Contact contact;
    private String policyNumber;

    public UserQuestionEntityBuilder setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
        return this;
    }

    public UserQuestionEntityBuilder setRequestText(String requestText) {
        this.requestText = requestText;
        return this;
    }

    public UserQuestionEntityBuilder setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public UserQuestionEntityBuilder setContact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public UserQuestionEntityBuilder setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
        return this;
    }


    public UserQuestion createUserQuestion() {
        return new UserQuestion(questionType, policyNumber, requestText, createdAt, contact);
    }
}