package com.hejda.lunde.interviewtest.model.dto;

import com.hejda.lunde.interviewtest.model.entity.QuestionType;

import java.time.LocalDateTime;

public class UserQuestionResponseDto {
    private final Long id;
    private final QuestionType questionType;
    private final String email;
    private final String policyNumber;
    private final String firstName;
    private final String lastName;
    private final String requestText;
    private final LocalDateTime createdAt;

    public UserQuestionResponseDto(
            Long id, QuestionType questionType,
            String email,
            String policyNumber,
            String firstName,
            String lastName,
            String requestText, LocalDateTime createdAt) {
        this.id = id;
        this.questionType = questionType;
        this.email = email;
        this.policyNumber = policyNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.requestText = requestText;
        this.createdAt = createdAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public String getEmail() {
        return email;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRequestText() {
        return requestText;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "UserQuestionResponseDto{" +
                "id=" + id +
                ", questionType=" + questionType +
                ", email='" + email + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", requestText='" + requestText + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
