package com.hejda.lunde.interviewtest.model.entity;

import javax.persistence.AttributeConverter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public enum QuestionType {
    CONTRACT_ADJUSTMENT(1, "Contract adjustment"),
    DAMAGE_CASE(2, "Damage case"),
    COMPLAINT(3, "Complaint");

    private static final Map<Integer, QuestionType> valueMap;

    static {
        Map<Integer, QuestionType> myMap = new HashMap<>();
        for (QuestionType questionType : values()) {
            myMap.put(questionType.id, questionType);
        }
        valueMap = Collections.unmodifiableMap(myMap);
    }

    private final int id;

    private final String formattedName;

    QuestionType(int id, String formattedName) {
        this.id = id;
        this.formattedName = formattedName;
    }

    public int getId() {
        return id;
    }

    public String getFormattedName() {
        return formattedName;
    }

    public static Map<String, String> getPossibleValues() {
        return valueMap.values().stream()
                .collect(Collectors.toMap(
                        Enum::name,
                        QuestionType::getFormattedName
                ));
    }

    @javax.persistence.Converter
    public static class Converter implements AttributeConverter<QuestionType, Integer> {

        @Override
        public Integer convertToDatabaseColumn(QuestionType questionType) {
            return questionType.id;
        }

        @Override
        public QuestionType convertToEntityAttribute(Integer dbData) {
            return valueMap.get(dbData);
        }

    }

}