package com.hejda.lunde.interviewtest.model.builder;

import com.hejda.lunde.interviewtest.model.entity.Contact;

public class ContactEntityBuilder {
    private String firstName;
    private String lastName;
    private String email;

    public ContactEntityBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ContactEntityBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public ContactEntityBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public Contact createContact() {
        return new Contact(firstName, lastName, email);
    }
}