package com.hejda.lunde.interviewtest.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hejda.lunde.interviewtest.model.entity.QuestionType;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserQuestionRequestDto {
    @NotNull
    private final QuestionType questionType;
    @Email
    private final String email;
    @Pattern(regexp = "[a-zA-Z0-9]*")
    private final String policyNumber;
    @Pattern(regexp = "[a-zA-Z]*")
    private final String firstName;
    @Pattern(regexp = "[a-zA-Z]*")
    private final String lastName;
    @Length(max = 500)
    private final String requestText;

    @JsonCreator
    public UserQuestionRequestDto(
            @JsonProperty("questionType") QuestionType questionType,
            @JsonProperty("email") String email,
            @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("firstName") String firstName,
            @JsonProperty("lastName") String lastName,
            @JsonProperty("requestText") String requestText) {
        this.questionType = questionType;
        this.email = email;
        this.policyNumber = policyNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.requestText = requestText;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public String getEmail() {
        return email;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRequestText() {
        return requestText;
    }

    @Override
    public String toString() {
        return "UserQuestionRequestDto{" +
                "questionType=" + questionType +
                ", email='" + email + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", requestText='" + requestText + '\'' +
                '}';
    }
}
