package com.hejda.lunde.interviewtest.model.builder;

import com.hejda.lunde.interviewtest.model.dto.UserQuestionResponseDto;
import com.hejda.lunde.interviewtest.model.entity.QuestionType;
import com.hejda.lunde.interviewtest.model.entity.UserQuestion;

import java.time.LocalDateTime;

public class UserQuestionResponseDtoBuilder {
    private QuestionType questionType;
    private String email;
    private String policyNumber;
    private String firstName;
    private String lastName;
    private String requestText;
    private LocalDateTime createdAt;
    private Long id;

    public UserQuestionResponseDtoBuilder setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
        return this;
    }

    public UserQuestionResponseDtoBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserQuestionResponseDtoBuilder setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
        return this;
    }

    public UserQuestionResponseDtoBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserQuestionResponseDtoBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserQuestionResponseDtoBuilder setRequestText(String requestText) {
        this.requestText = requestText;
        return this;
    }

    public UserQuestionResponseDtoBuilder setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public UserQuestionResponseDtoBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public UserQuestionResponseDto createUserQuestionResponseDto() {
        return new UserQuestionResponseDto(id, questionType, email, policyNumber, firstName, lastName, requestText, createdAt);
    }

    public static UserQuestionResponseDto fromEntity(UserQuestion entity) {
        return new UserQuestionResponseDto(
                entity.getId(),
                entity.getQuestionType(),
                entity.getContact().getEmail(),
                entity.getPolicyNumber(),
                entity.getContact().getFirstName(),
                entity.getContact().getLastName(),
                entity.getRequestText(),
                entity.getCreatedAt()
        );
    }
}