package com.hejda.lunde.interviewtest.dao;

import com.hejda.lunde.interviewtest.model.entity.UserQuestion;
import org.springframework.data.repository.CrudRepository;

public interface UserQuestionRepository extends CrudRepository<UserQuestion, Long> {
}
