package com.hejda.lunde.interviewtest.dao;

import com.hejda.lunde.interviewtest.model.entity.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ContactRepository extends CrudRepository<Contact, Long> {
    /**
     * Returns a Optional of Contact entity searched by an email
     * @param email email used to search the contact
     * @return The contact entity, if any matches
     */
    Optional<Contact> findContactByEmail(String email);
}
