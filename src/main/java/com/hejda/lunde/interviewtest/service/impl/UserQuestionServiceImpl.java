package com.hejda.lunde.interviewtest.service.impl;

import com.hejda.lunde.interviewtest.dao.ContactRepository;
import com.hejda.lunde.interviewtest.dao.UserQuestionRepository;
import com.hejda.lunde.interviewtest.exception.ErrorCode;
import com.hejda.lunde.interviewtest.exception.ITException;
import com.hejda.lunde.interviewtest.model.builder.ContactEntityBuilder;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionEntityBuilder;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionResponseDtoBuilder;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionRequestDto;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionResponseDto;
import com.hejda.lunde.interviewtest.model.entity.Contact;
import com.hejda.lunde.interviewtest.model.entity.UserQuestion;
import com.hejda.lunde.interviewtest.service.UserQuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class UserQuestionServiceImpl implements UserQuestionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserQuestionServiceImpl.class);

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private UserQuestionRepository userQuestionRepository;

    @Transactional
    public UserQuestionResponseDto insertUserQuestion(final UserQuestionRequestDto requestDto) {
        LOGGER.info("Processing request {}", requestDto);

        final Contact contact = contactRepository.findContactByEmail(requestDto.getEmail())
                .orElse(new ContactEntityBuilder()
                        .setEmail(requestDto.getEmail())
                        .createContact()
                );
        contact.setFirstName(requestDto.getFirstName());
        contact.setLastName(requestDto.getLastName());

        final UserQuestion userQuestion = new UserQuestionEntityBuilder()
                .setContact(contact)
                .setCreatedAt(LocalDateTime.now())
                .setQuestionType(requestDto.getQuestionType())
                .setRequestText(requestDto.getRequestText())
                .setPolicyNumber(requestDto.getPolicyNumber())
                .createUserQuestion();

        final UserQuestionResponseDto userQuestionResponseDto = UserQuestionResponseDtoBuilder.fromEntity(userQuestionRepository.save(userQuestion));

        LOGGER.debug("Saved UserRequest as: {}", userQuestionResponseDto);
        return userQuestionResponseDto;
    }

    public UserQuestionResponseDto getUserQuestion(Long id) {
        LOGGER.info("Getting UserQuestion id: {}", id);
        final UserQuestionResponseDto userQuestionResponseDto = UserQuestionResponseDtoBuilder.fromEntity(userQuestionRepository.findById(id)
                .orElseThrow(() -> new ITException(ErrorCode.QUESTION_NOT_FOUND)));

        LOGGER.debug("Retrieved UserQuestion {}", userQuestionResponseDto);
        return userQuestionResponseDto;
    }
}
