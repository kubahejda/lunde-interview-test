package com.hejda.lunde.interviewtest.service;

import com.hejda.lunde.interviewtest.model.dto.UserQuestionRequestDto;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionResponseDto;

/**
 * A service for the UserQuestion management
 */
public interface UserQuestionService {
    /**
     * Inserts a UserQuestion into the DB. Reuses already inserted contact if finds
     * one by email.
     * @param requestDto Incomming request
     * @return Inserted data
     */
    UserQuestionResponseDto insertUserQuestion(final UserQuestionRequestDto requestDto);

    /**
     * Retrieves an UserQuestion by its id
     * @param id id of the entity
     * @return UserQuestion data
     */
    UserQuestionResponseDto getUserQuestion(Long id);
}
