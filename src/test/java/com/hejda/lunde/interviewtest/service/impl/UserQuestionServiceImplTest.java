package com.hejda.lunde.interviewtest.service.impl;

import com.hejda.lunde.interviewtest.dao.UserQuestionRepository;
import com.hejda.lunde.interviewtest.exception.ITException;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionEntityBuilder;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionRequestDtoBuilder;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionResponseDtoBuilder;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionRequestDto;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionResponseDto;
import com.hejda.lunde.interviewtest.model.entity.Contact;
import com.hejda.lunde.interviewtest.model.entity.QuestionType;
import com.hejda.lunde.interviewtest.model.entity.UserQuestion;
import com.hejda.lunde.interviewtest.service.UserQuestionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback
public class UserQuestionServiceImplTest {

    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String REQUEST_TEXT = "text";
    private static final QuestionType QUESTION_TYPE = QuestionType.COMPLAINT;
    private static final String POLICY_NUMBER = "policyNumber";
    private static final Long FAKE_ID = 999L;

    @Autowired
    private UserQuestionRepository userQuestionRepository;

    @Autowired
    private UserQuestionService userQuestionService;

    @Test
    public void insertUserQuestion_success() {
        final UserQuestionRequestDto userQuestionRequestDto = new UserQuestionRequestDtoBuilder()
                .setEmail(EMAIL)
                .setFirstName(FIRST_NAME)
                .setLastName(LAST_NAME)
                .setPolicyNumber(POLICY_NUMBER)
                .setQuestionType(QUESTION_TYPE)
                .setRequestText(REQUEST_TEXT)
                .createUserQuestionRequestDto();

        final UserQuestionResponseDto expectedDto = userQuestionService.insertUserQuestion(userQuestionRequestDto);

        final UserQuestion actualEntity = userQuestionRepository.findById(expectedDto.getId()).orElse(null);
        assertNotNull(actualEntity);
        assertNotNull(actualEntity.getContact());

        final UserQuestionResponseDto actualDto = UserQuestionResponseDtoBuilder.fromEntity(actualEntity);

        assertThat(actualDto, samePropertyValuesAs(expectedDto));
    }

    @Test
    public void getUserQuestion_success() {
        final UserQuestion userQuestion = new UserQuestionEntityBuilder()
                .setContact(new Contact(FIRST_NAME, LAST_NAME, EMAIL))
                .setPolicyNumber(POLICY_NUMBER)
                .setQuestionType(QUESTION_TYPE)
                .setRequestText(REQUEST_TEXT)
                .createUserQuestion();

        userQuestionRepository.save(userQuestion);

        final UserQuestionResponseDto expectedDto =
                new UserQuestionResponseDto(userQuestion.getId(), QUESTION_TYPE, EMAIL, POLICY_NUMBER, FIRST_NAME, LAST_NAME, REQUEST_TEXT, userQuestion.getCreatedAt());
        final UserQuestionResponseDto actualDto = userQuestionService.getUserQuestion(userQuestion.getId());

        assertNotNull(actualDto);
        assertThat(actualDto, samePropertyValuesAs(expectedDto));
    }

    @Test(expected = ITException.class)
    @Rollback
    public void getUserQuestion_notFound_negative() {
        userQuestionService.getUserQuestion(FAKE_ID);
    }
}