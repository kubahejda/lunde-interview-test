package com.hejda.lunde.interviewtest.controller.v1;

import com.hejda.lunde.interviewtest.dao.UserQuestionRepository;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionEntityBuilder;
import com.hejda.lunde.interviewtest.model.builder.UserQuestionRequestDtoBuilder;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionRequestDto;
import com.hejda.lunde.interviewtest.model.dto.UserQuestionResponseDto;
import com.hejda.lunde.interviewtest.model.entity.Contact;
import com.hejda.lunde.interviewtest.model.entity.QuestionType;
import com.hejda.lunde.interviewtest.model.entity.UserQuestion;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.request.ServletWebRequest;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserQuestionControllerTest {

    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String REQUEST_TEXT = "text";
    private static final QuestionType QUESTION_TYPE = QuestionType.COMPLAINT;
    private static final String POLICY_NUMBER = "policyNumber";

    @Autowired
    private UserQuestionController userQuestionController;

    @Autowired
    private UserQuestionRepository userQuestionRepository;

    @Test
    public void insertUserRequest_success() {
        final UserQuestionRequestDto userQuestionRequestDto = new UserQuestionRequestDtoBuilder()
                .setEmail(EMAIL)
                .setFirstName(FIRST_NAME)
                .setLastName(LAST_NAME)
                .setPolicyNumber(POLICY_NUMBER)
                .setQuestionType(QUESTION_TYPE)
                .setRequestText(REQUEST_TEXT)
                .createUserQuestionRequestDto();


        final ServletWebRequest webRequest = new ServletWebRequest(new MockHttpServletRequest());
        final ResponseEntity<UserQuestionResponseDto> response = userQuestionController.insertUserQuestion(userQuestionRequestDto, webRequest);
        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        final UserQuestionResponseDto actualDto = response.getBody();
        assertNotNull(actualDto);

        final UserQuestionResponseDto expectedDto =
                new UserQuestionResponseDto(actualDto.getId(), QUESTION_TYPE, EMAIL, POLICY_NUMBER, FIRST_NAME, LAST_NAME, REQUEST_TEXT, actualDto.getCreatedAt());
        assertThat(actualDto, samePropertyValuesAs(expectedDto));

    }

    @Test
    public void getUserRequest_success() {
        final UserQuestion userQuestion = new UserQuestionEntityBuilder()
                .setContact(new Contact(FIRST_NAME, LAST_NAME, EMAIL))
                .setPolicyNumber(POLICY_NUMBER)
                .setQuestionType(QUESTION_TYPE)
                .setRequestText(REQUEST_TEXT)
                .createUserQuestion();

        userQuestionRepository.save(userQuestion);

        final UserQuestionResponseDto expectedDto =
                new UserQuestionResponseDto(userQuestion.getId(), QUESTION_TYPE, EMAIL, POLICY_NUMBER, FIRST_NAME, LAST_NAME, REQUEST_TEXT, userQuestion.getCreatedAt());

        final ResponseEntity<UserQuestionResponseDto> response = userQuestionController.getUserRequest(userQuestion.getId());
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        final UserQuestionResponseDto actualDto = response.getBody();
        assertNotNull(actualDto);
        assertThat(actualDto, samePropertyValuesAs(expectedDto));
    }

    /*
        Because @Rollback does not work when @Transactional used in methods at the lower level
     */
    @After
    public void cleanDB() {
        userQuestionRepository.deleteAll();
    }
}